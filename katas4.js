const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";
// 1 Escreva uma função que retorna um array com as cidades em 'gotCitiesCSV'.
//  Lembre-se de também adicionar os resultados à página.
let header = document.createElement("div");
header.textContent = "Kata 1";
document.body.appendChild(header);



function kata1() {
   let newElement = document.createElement("div");
   newElement.textContent = JSON.stringify(gotCitiesCSV.split());
   document.body.appendChild(newElement)

   return gotCitiesCSV; // Não esqueça de retornar a saída
}
kata1()
// 2 Escreva uma função que retorna um array das palavras na frase contida em 'bestThing'.
//  Lembre-se de também adicionar os resultados à página.
let header2 = document.createElement("div");
header2.textContent = "Kata 2";
document.body.appendChild(header2);

function kata2() {
   let newElement2 = document.createElement("div");
   newElement2.textContent = JSON.stringify(bestThing.split(' '));
   document.body.appendChild(newElement2)

   return bestThing; // Não esqueça de retornar a saída
}
kata2()
// 3 Escreva uma função que retorna uma string separada por ponto-e-vírgulas em vez das vírgulas de 'gotCitiesCSV'.
//  Lembre-se de também adicionar os resultados à página.
let header3 = document.createElement("div");
header3.textContent = "Kata 3";
document.body.appendChild(header3);

function kata3() {
   let newElement3 = document.createElement("div");
   newElement3.textContent = JSON.stringify(gotCitiesCSV.replace(/,/g, '; '));
   document.body.appendChild(newElement3)

   return gotCitiesCSV; // Não esqueça de retornar a saída
}
kata3()
//  4  Escreva uma função que retorne uma string CSV (separada por vírgulas) de 'lotrCitiesArray'.
//  Lembre-se de também adicionar os resultados à página.
let header4 = document.createElement("div");
header4.textContent = "Kata 4";
document.body.appendChild(header4);

function kata4() {
   let newElement4 = document.createElement("div");
   newElement4.textContent = JSON.stringify(lotrCitiesArray.toString());
   document.body.appendChild(newElement4)

   return lotrCitiesArray; // Não esqueça de retornar a saída
}
kata4()
// 5 Escreva uma função que retorna um array com as 5 primeiras cidades de 'lotrCitiesArray'.
// Lembre-se de também adicionar os resultados à página.

let header5 = document.createElement("div");
header5.textContent = "Kata 5";
document.body.appendChild(header5);

function kata5() {

   let newElement5 = document.createElement("div");
   newElement5.textContent = JSON.stringify(lotrCitiesArray.slice(0, 5));
   document.body.appendChild(newElement5)

   return lotrCitiesArray; // Não esqueça de retornar a saída
}
kata5()
// 6 Escreva uma função que retorna um array com as 5 últimas cidades de 'lotrCitiesArray'.
// Lembre-se de também adicionar os resultados à página.
let header6 = document.createElement("div");
header6.textContent = "Kata 6";
document.body.appendChild(header6);

function kata6() {
   let cidade = lotrCitiesArray.slice(3, 8)
   let newElement6 = document.createElement("div");
   newElement6.textContent = JSON.stringify(cidade);
   document.body.appendChild(newElement6)

   return cidade; // Não esqueça de retornar a saída
}
kata6()

// 7 Escreva uma função que retorna um array contendo da 3ª a 5ª cidades de 'lotrCitiesArray'.
// Lembre-se de também adicionar os resultados à página.

let header7 = document.createElement("div");
header7.textContent = "Kata 7";
document.body.appendChild(header7);

function kata7() {


   let newElement7 = document.createElement("div");
   newElement7.textContent = JSON.stringify(lotrCitiesArray.splice(2, 3, "Rohan", "Beleriand", "Mirkwood"));
   document.body.appendChild(newElement7)

   return; // Não esqueça de retornar a saída
}
kata7()
// 8 Escreva uma função que use 'splice' para remover 'Rohan' de 'lotrCitiesArray' e retorne o novo 
// 'lotrCitiesArray' modificado. Lembre-se de também adicionar os resultados à página.

let header8 = document.createElement("div");
header8.textContent = "Kata 8";
document.body.appendChild(header8);

function kata8() {
   let cidades2 = lotrCitiesArray.splice(2, 1);
   let newElement8 = document.createElement("div");
   newElement8.textContent = JSON.stringify(lotrCitiesArray);
   document.body.appendChild(newElement8)

   return cidades2; // Não esqueça de retornar a saída
}
kata8()
// 9 Escreva uma função que use 'splice' para remover todas as cidades depois de 'Dead Marshes' de 'lotrCitiesArray' 
// e retorne o novo 'lotrCitiesArray' modificado. Lembre-se de também adicionar os resultados à página.
let header9 = document.createElement("div");
header9.textContent = "Kata 9";
document.body.appendChild(header9);

function katas9() {
   let cidade3 = lotrCitiesArray.splice(5, 2)
   let newElement9 = document.createElement("div");
   newElement9.textContent = JSON.stringify(lotrCitiesArray);
   document.body.appendChild(newElement9)

   return cidade3; // Não esqueça de retornar a saída
}
katas9()
// 10   Escreva uma função que use 'splice' para adicionar 'Rohan' de volta ao 'lotrCitiesArray'
//     logo depois de 'Gondor' e retorne o novo 'lotrCitiesArray'

let header10 = document.createElement("div");
header10.textContent = "Kata 10";
document.body.appendChild(header10);
function kata10() {
   let cidades4 = lotrCitiesArray.splice(2, 0, "Rohan")
   let newElement10 = document.createElement("div");
   newElement10.textContent = JSON.stringify(lotrCitiesArray);
   document.body.appendChild(newElement10)

   return lotrCitiesArray; // Não esqueça de retornar a saída
}
kata10()
// katas 11 Escreva uma função que use 'splice' para renomear 'Dead Marshes' para 'Deadest Marshes'em 'lotrCitiesArray'
//  e retorne o novo 'lotrCitiesArray' modificado. Lembre-se de também adicionar os resultados à página

let header11 = document.createElement("div");
header11.textContent = "Kata 11";
document.body.appendChild(header11);

function kata11() {
   let cidades11 = lotrCitiesArray.splice(5, 5, "Deadest Marshes")
   let newElement11 = document.createElement("div");
   newElement11.textContent = JSON.stringify(lotrCitiesArray);
   document.body.appendChild(newElement11)

   return cidades11; // Não esqueça de retornar a saída
}
kata11()
// Escreva uma função que usa 'slice' para retornar uma string com os primeiros 14 caracteres de 'bestThing'.
//  Lembre-se de também adicionar os resultados à página.
let header12 = document.createElement("div");
header12.textContent = "Kata 12";
document.body.appendChild(header12);

function kata12() {

   let newElement12 = document.createElement("div");
   newElement12.textContent = JSON.stringify(bestThing.split('').slice(0, 14));
   document.body.appendChild(newElement12)

   return bestThing; // Não esqueça de retornar a saída
}
kata12()

// 13 Escreva uma função que usa 'slice' para retornar uma string com os 12 últimos caracteres de 'bestThing'.
//  Lembre-se de também adicionar os resultados à página.

let header13 = document.createElement("div");
header13.textContent = "Kata 13";
document.body.appendChild(header13);

function kata13() {

   let newElement13 = document.createElement("div");
   newElement13.textContent = JSON.stringify(bestThing.split('').slice(69, 82));
   document.body.appendChild(newElement13)

   return bestThing; // Não esqueça de retornar a saída
}
kata13()

// 14 Escreva uma função que usa 'slice' para retornar uma string com os caracteres entre as posições 23 e 38 de 'bestThing'
//  (ou seja, 'booleano é par'). Lembre-se de também adicionar os resultados à página.

let header14 = document.createElement("div");
header14.textContent = "Kata 14";
document.body.appendChild(header14);

function kata14() {

   let newElement14 = document.createElement("div");
   newElement14.textContent = JSON.stringify(bestThing.split('').slice(23, 38));
   document.body.appendChild(newElement14)

   return bestThing; // Não esqueça de retornar a saída
}
kata14()

// 15 Escreva uma função que faz exatamente a mesma coisa que a #13 mas use o método 'substring' em vez de 'slice'.
//  Lembre-se de também adicionar os resultados à página.

let header15 = document.createElement("div");
header15.textContent = "Kata 15";
document.body.appendChild(header15);

function kata15() {

   let newElement15 = document.createElement("div");
   newElement15.textContent = JSON.stringify(bestThing.substring(0, 14));
   document.body.appendChild(newElement15)

   return bestThing; // Não esqueça de retornar a saída
}
kata15()

//16 Escreva uma função que faça exatamente a mesma coisa que o #14
//  mas use o método 'substring' em vez de 'slice'. Lembre-se de também adicionar os resultados à página.

let header16 = document.createElement("div");
header16.textContent = "Kata 16";
document.body.appendChild(header16);

function kata16() {

   let newElement16 = document.createElement("div");
   newElement16.textContent = JSON.stringify(bestThing.substring(23, 38));
   document.body.appendChild(newElement16)

   return bestThing; // Não esqueça de retornar a saída
}
kata16()

//17 Escreva uma função que use 'pop' para remover a última cidade de 'lotrCitiesArray e retorne o novo array.
//  Lembre-se de também adicionar os resultados à página.

let header17 = document.createElement("div");
header17.textContent = "Kata 17";
document.body.appendChild(header17);

function kata17() {
   let cidade17 = lotrCitiesArray.pop()
   let newElement17 = document.createElement("div");
   newElement17.textContent = JSON.stringify(lotrCitiesArray);
   document.body.appendChild(newElement17)

   return cidade17; // Não esqueça de retornar a saída
}
kata17()
//18 Escreva uma função que usa 'push' para adicionar de volta, no final do array, a cidade de 'lotrCitiesArray'
//  que foi removida no #17 e retorne o novo array. Lembre-se de também adicionar os resultados à página.

let header18 = document.createElement("div");
header18.textContent = "Kata 18";
document.body.appendChild(header18);

function kata18() {
   let cidade5 = lotrCitiesArray.push("Deadest Marshes")
   let newElement18 = document.createElement("div");
   newElement18.textContent = JSON.stringify(lotrCitiesArray);
   document.body.appendChild(newElement18)

   return lotrCitiesArray; // Não esqueça de retornar a saída
}
kata18()
// Escreva uma função que usa 'shift' para remover a primeira cidade de
// 'lotrCitiesArray e retorne o novo array. Lembre-se de também adicionar os resultados à página.
let header19 = document.createElement("div");
header19.textContent = "Kata 19";
document.body.appendChild(header19);

function kata19() {
   let cidades19 = lotrCitiesArray.shift()
   let newElement19 = document.createElement("div");
   newElement19.textContent = JSON.stringify(lotrCitiesArray);
   document.body.appendChild(newElement19)

   return cidades19; // Não esqueça de retornar a saída
}
kata19()
// 20 Escreva uma função que use 'unshift' para adicionar de volta, no começo do array, a cidade de 'lotrCitiesArray'
//  que foi removida no #19 e retorne o novo array. Lembre-se de também adicionar os resultados à página.
let header20 = document.createElement("div");
header20.textContent = "Kata 20";
document.body.appendChild(header20);

function kata20() {
   let cidade6 = lotrCitiesArray.unshift('Mordor')
   let newElement20 = document.createElement("div");
   newElement20.textContent = JSON.stringify(lotrCitiesArray);
   document.body.appendChild(newElement20)

   return cidade6; // Não esqueça de retornar a saída
}
kata20()
//bonus 1 Escreva uma função que encontre e retorne o índice de 'only' em 'bestThing'.
// Lembre-se de também adicionar os resultados à página

let headerbonus1 = document.createElement("div");
headerbonus1.textContent = "bonuskata1";
document.body.appendChild(headerbonus1);

function bonus1() {

   let newElementbous1 = document.createElement("div");
   newElementbous1.textContent = JSON.stringify(bestThing.indexOf('only'));
   document.body.appendChild(newElementbous1)

   return bestThing; // Não esqueça de retornar a saída
}
bonus1()

// bonus 2 Escreva uma função que encontre e retorne o índice da última palavra de 'bestThing'.
//  Lembre-se de também adicionar os resultados à página.

let headerbonus2 = document.createElement("div");
headerbonus2.textContent = "bonuskata2";
document.body.appendChild(headerbonus2);

function bonus2() {

   let newElementbonus2 = document.createElement("div");
   newElementbonus2.textContent = JSON.stringify(bestThing.split(/\s/).indexOf('bit'));
   document.body.appendChild(newElementbonus2)

   return bestThing; // Não esqueça de retornar a saída
}
bonus2()

// bonus 3 Escreva uma função que encontre e retorne um array de todas as cidades de 'gotCitiesCSV' que tiverem vogais duplicadas ('aa', 'ee', etc.).
//  Lembre-se de também adicionar os resultados à página. 

let headerbonus3 = document.createElement("div");
headerbonus3.textContent = "bonuskata3";
document.body.appendChild(headerbonus3);

function bonus3() {
   let vogais = gotCitiesCSV.split(',');
   vogais.toString();
   let array = ['aa', 'ee', 'ii', 'oo', 'uu']
   let array2 = []
   for (let i = 0; i < vogais.length; i++) {
      for (let j = 0; j < array.length; j++) {
         if (vogais[i].includes(array[j])) {
            array2.push(vogais[i]);

         }
      }
   }
   //   loop aninhado, array gotcities + array vogais
   // 

   let newElementbonus3 = document.createElement("div");
   newElementbonus3.textContent = JSON.stringify(array2);
   document.body.appendChild(newElementbonus3)

   return array2; // Não esqueça de retornar a saída
}
bonus3()
// Escreva uma função que encontre e retorne um array com todas as cidades de 'lotrCitiesArray' que terminem em 'or'.
//  Lembre-se de também adicionar os resultados à página.

let headerbonus4 = document.createElement("div");
headerbonus4.textContent = "bonuskata4";
document.body.appendChild(headerbonus4);

function bonus4() {
   let vogais2 = lotrCitiesArray.split('')
   vogais.toString()
   let bonus4 = ['or']
   let array3 = []

   for (let i = 0; i < vogais2.length; i++) {
      for (let j = 0; j < bonus4.length; j++) {
         if (vogais2[i].includes(bonus4[j])) {
            array3.push(vogais2[i]);

         }
      }
   }
   let newElementbonus4 = document.createElement("div");
   newElementbonus4.textContent = JSON.stringify(array3);
   document.body.appendChild(newElementbonus4)

   return array3; // Não esqueça de retornar a saída
}
bonus4()


// let headerbonus5 = document.createElement("div");
//    headerbonus5.textContent = "bonuskata5";
//    document.body.appendChild(headerbonus5);

// function bonus5() {
//    let comeca = bestThing.split(',')
//    comeca.forEach(
//       (element = (element, index) => {
//          let best = element[0]
//          if(
//             element[0].includes['b'] === true ||
//             element[0].includes('B') ===  true
//          ) {
//             return (element, index)
//       }
//    })
//    )

//    let newElementbonus4 = document.createElement("div");
//     newElementbonus4.textContent = JSON.stringify(bestThing);
//     document.body.appendChild(newElementbonus4)

//     return bestThing; // Não esqueça de retornar a saída
// }
// bonus5()

// let headerbonus6 = document.createElement("div");
//    headerbonus6.textContent = "bonuskata5";
//    document.body.appendChild(headerbonus6);

// function bonus6() {
//    let simounao = lotrCitiesArray
//    if(simounao.includes('Hollywood') === true){
//          return true
//    } 

//    let newElementbonus6 = document.createElement("div");
//     newElementbonus6.textContent = JSON.stringify(lotrCitiesArray);
//     document.body.appendChild(newElementbonus6)

//     return lotrCitiesArray; // Não esqueça de retornar a saída
// }
// bonus7()

// let headerbonus6 = document.createElement("div");
//    headerbonus6.textContent = "bonuskata5";
//    document.body.appendChild(headerbonus6);

// function bonus7() {
//    let simounao = lotrCitiesArray.indexOf('Mirkwood')

//    let newElementbonus6 = document.createElement("div");
//     newElementbonus6.textContent = JSON.stringify(lotrCitiesArray);
//     document.body.appendChild(newElementbonus6)

//     return lotrCitiesArray; // Não esqueça de retornar a saída
// }
// bonus7()

// let headerbonus6 = document.createElement("div");
//    headerbonus6.textContent = "bonuskata5";
//    document.body.appendChild(headerbonus6);

// function bonus9() {
//    let sduaspalavras = lotrCitiesArray
//     sduaspalavras.forEach(element= (element, index)=> {
//       if(element.includes(' ') === true) {
//         return (element, index)
//       } 
//     });

//     let newElementbonus6 = document.createElement("div");
//     newElementbonus6.textContent = JSON.stringify(lotrCitiesArray);
//     document.body.appendChild(newElementbonus6)

//     return lotrCitiesArray; // Não esqueça de retornar a saída
// }
// bonus9()
// let headerbonus6 = document.createElement("div");
//    headerbonus6.textContent = "bonuskata5";
//    document.body.appendChild(headerbonus6);

// function bonus10() {
//    let revert = lotrCitiesArray.reverse()

//    let newElementbonus6 = document.createElement("div");
//     newElementbonus6.textContent = JSON.stringify(lotrCitiesArray);
//     document.body.appendChild(newElementbonus6)

//     return lotrCitiesArray; // Não esqueça de retornar a saída
// }
// bonus10()

// let headerbonus6 = document.createElement("div");
//    headerbonus6.textContent = "bonuskata5";
//    document.body.appendChild(headerbonus6);

// function bonus11() {
//    let rordem = lotrCitiesArray.sort()

//    let newElementbonus6 = document.createElement("div");
//     newElementbonus6.textContent = JSON.stringify(lotrCitiesArray);
//     document.body.appendChild(newElementbonus6)

//     return lotrCitiesArray; // Não esqueça de retornar a saída
// }
// bonus11()


// let headerbonus6 = document.createElement("div");
//    headerbonus6.textContent = "bonuskata5";
//    document.body.appendChild(headerbonus6);

// function bonus12() {
//    const comparacao = (a,b) => {
//       return a.length > b.length;
//    };
//    let organizar = lotrCitiesArray
//    // organizar.forEach(element = (element, index) => {
//    //    element.sort(comparacao(element.element[index+1]))
//    //    let a = [].slice.call(arguments.sort(function (element,element[index + 1]));
// // ARRUMAR

//    let newElementbonus6 = document.createElement("div");
//     newElementbonus6.textContent = JSON.stringify(ordemenor);
//     document.body.appendChild(newElementbonus6)

//     return lotrCitiesArray; // Não esqueça de retornar a saída
// }
// bonus12()





































